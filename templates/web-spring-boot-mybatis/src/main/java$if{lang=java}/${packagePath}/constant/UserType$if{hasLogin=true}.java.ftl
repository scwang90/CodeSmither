package ${packageName}.constant;

/**
 * 用户类型
 * @author ${author}
 * @since ${now?string("yyyy-MM-dd zzzz")}
 */
public enum UserType implements ShortEnum {
    User, Admin
}
