package ${packageName}.model;

/**
 * 所有数据模型接口
 * @author ${author}
 * @since ${now?string("yyyy-MM-dd zzzz")}
 */
public interface Bean {
}
