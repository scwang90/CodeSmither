package ${packageName}.model;

/**
 * 所有通用数据库 Model
 * @author ${author}
 * @since ${now?string("yyyy-MM-dd zzzz")}
 */
public abstract class Entity implements Bean {
}
