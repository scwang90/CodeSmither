package ${packageName}.constant;

/**
 * 用户类型
 * @author ${author}
 * @since ${now?string("yyyy-MM-dd zzzz")}
 */
enum class UserType {
    User, Admin
}
