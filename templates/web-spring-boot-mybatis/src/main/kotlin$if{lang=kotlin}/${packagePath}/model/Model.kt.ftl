package ${packageName}.model

/**
 * 所有通用 Api Model
 * @author ${author}
 * @since ${now?string("yyyy-MM-dd zzzz")}
 */
abstract class Model : Bean